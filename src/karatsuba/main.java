package karatsuba;

import java.math.BigInteger;
import java.util.Random;

public class main {

	public static void main(String[] args) {

		BigInteger n = new BigInteger ("1653452342156769878987978676521320198879987897987789799");
		BigInteger z = new BigInteger ("877987987984654658798798798787632165498759");
		BigInteger R = n.multiply(z);
		System.out.print("n "+n);
		System.out.println(" * z "+z);
		System.out.println("Resultado "+R);
		System.out.println("Resultado longitud "+R.bitLength());
		
//		Random random = new Random(0);
//		BigInteger p = new BigInteger(270, random);
//		System.out.println(" P "+p);
		System.out.println("Multiplicar "+multiplicar(n,z));


/*
 *
 * partir al medio
 v! x = x 10m + x2 e y = y 10m + y2.
 v!A = x1 * y1;
 v!B = x2 * y2;
 v!C = (x1 + x2) * (y1 + y2);
 v!K = C - A - B; (Notar que K = x1y2 + x2y1)
 Resultado = A * 102m + K * 10m + B;
 */
	}
	public static BigInteger multiplicar (BigInteger x, BigInteger y)
	{
		int m = Math.max(x.bitLength(), y.bitLength()) /2;
		if (m <= 10)
			return x.multiply(y);
		
		BigInteger x1 = x.shiftRight(m); 
		BigInteger x2 = x.subtract(x1.shiftLeft(m));
		BigInteger y1 = y.shiftRight(m);
		BigInteger y2 = y.subtract(y1.shiftLeft(m));

		
		BigInteger A = multiplicar(x1, y1);
		BigInteger B = multiplicar(x2, y2);
		BigInteger C = multiplicar(x1.add(x2), y1.add(y2)) ;
		BigInteger K = (C.subtract(A)).subtract(B);
		
	
		return A.shiftLeft(2*m).add(K.shiftLeft(m).add(B));  
	}

}
